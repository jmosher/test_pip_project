#!/usr/bin/env python

from datetime import datetime
import os
import os.path
import sys


template = """<html>
<head><title>Index of {parent}</title></head>
<body bgcolor="white">
<h1>Index of {parent}</h1><hr><pre><a href="../">../</a>
{links}
</pre><hr></body>
</html>
"""

link_template = """<a href="{filename}">{filename}</a>{filename_padding}{datetime}{filesize:>20}"""


def main():
    start_dir = sys.argv[1]

    for ds in os.walk(start_dir):
        index = gen_index(ds, strip='public')
        with open(ds[0] + '/index.html', 'w') as fp:
            fp.write(index)


def gen_index(ds, strip=None):
    basepath = ds[0] + '/'
    links = []

    for d in ds[1]:
        dirname = basepath + d
        data = os.stat(dirname)
        if strip and dirname.startswith(strip + '/'):
            dirname = dirname[len(strip) + 1:]
        links.append(link_template.format(
                filename=dirname + '/',
                filename_padding=' ' * (51 - len(dirname) - 1),
                datetime=datetime.fromtimestamp(data.st_mtime).strftime("%Y-%m-%d %H:%S"),
                filesize='-'))

    for f in ds[2]:
        if f == 'index.html':
            continue

        fname = basepath + f
        data = os.stat(fname)
        if strip and fname.startswith(strip + '/'):
            fname = fname[len(strip) + 1:]
        links.append(link_template.format(
                filename=os.path.basename(fname),
                filename_padding=' ' * (51 - len(os.path.basename(fname))),
                datetime=datetime.fromtimestamp(data.st_mtime).strftime("%Y-%m-%d %H:%S"),
                filesize=data.st_size))

    if strip and basepath.startswith(strip):
        parent = basepath[len(strip):]
    else:
        parent = basepath

    return template.format(parent=parent, 
                           links="\n".join(links))


if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        pass
