#!/usr/bin/env python

import pymysql


_db_pool = {}
_db_creds = {}


def get_dbh(name):
    if name in _db_pool:
        return _db_pool[name]
    elif name in _db_creds:
        dbh = pymysql.connect(**_db_creds[name])
        _db_pool[name] = dbh
        return dbh
    else:
        return None


def register_creds(name, **kwargs):
    creds = {'host': kwargs.get('host'),
             'user': kwargs.get('user'),
             'passwd': kwargs.get('passwd'),
             'db': kwargs.get('db'),
             'charset': kwargs.get('charset', 'utf8mb4'),
             'cursorclass': kwargs.get('cursorclass')
            }
    _db_creds[name] = creds
